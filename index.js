/* Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)*/
/*Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…*/

const getCube = (num) => {
		let numCube = Math.pow(num, 3);
		console.log(`The cube of ${num} is ${numCube}`);
	};

	getCube(2);


/* Create a variable address with a value of an array containing details of an address.*/

const address = ["258 Washington Ave NW", "Californa", "90011"];

/*Destructure the array and print out a message with the full address using Template Literals.*/

const [streetName, state, zip] = address;

console.log(`I live at ${streetName}, ${state} ${zip}`);

/* Create a variable animal with a value of an object data type with different animal details as it’s properties.*/
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: 1075,
	heightFt: 20,
	heightIn: 3,
}

/*8. Destructure the object and print out a message with the details of the animal using Template Literals.*/

console.log(`${animal.name} is a ${animal.species}. He weighed at ${animal.weight} kgs with a measurement of ${animal.heightFt} ft ${animal.heightIn}.`);

/* Create an array of numbers.*/

const numbers = [1, 2, 3, 4, 5];

/* Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/


	numbers.forEach((number) => {
		console.log(`${number}`);
	});

/*Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.*/

const initialValue = 0;
const reduceNumber = numbers.reduce(
  (x, y) => x + y, initialValue);

console.log(reduceNumber);

/*12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.*/

class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const myNewdog = new dog("Frankie", 5, "Miniature Dachschund");
console.log(myNewdog);
